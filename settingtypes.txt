
# if enabled will Kill with one hit only when antipvp are grant
antipvp_kill_hitter (Kill hitter if player has antipvp privilegies) bool false

# if enable admin will be pvp invulnerable
antipvp_admin_privs (Give automatically to the admin of server pvp invulnerable privs) bool true

# if enabled send the notification of hostile actions agais a player, to hitter player
antipvp_send_notify (Send or not message notifiacion in chat of those involved) bool true

# if enabled send the activity of hurt to invulnerable players to all the chats!
antipvp_send_to_all (Send message to all the players and not only those involved) bool false

# amoun of bloc around the peacefull block that give protection to player
antipvp_area_ratio (Ratio in blocks of the peacefull antipvp block) int 3
