minetest mod antipvp
=====================

Provides pvp invulnerability to players by privilegie.

Information
-----------

This mod is named `antipvp` and add a priv that give PvP(Player vs Player) invulnerability.

![screenshot.png](screenshot.png)


## tech information

This is a pvp invulnerability mod based on most others, mostly the most is
part of the venenux minenux server antimods mod, but in single simplified way.

* do not hit performance of the server and the source code is more 
didactically to learn by some users that wanst to modify (Check license please).
* it feature antipvp block that depending of configuration, on anarchy servers 
can be a tramp or a hope, use with care
* the message event is configurable so it doesn't flood the chat for every hit 
on multiple attacks (to all players or only involved)
* It provides backguard compatibility and stability for older engines, so 
we avoid the pain of upgrades with stupids reasons by developers

It provides a way to grant pvp god privilegies agains pvp(Player vs Player) battles:

`/grant <player> antipvp`

The `PVP` term means player versus player.

### Download

Can be downloaded from https://codeberg.org/minenux/minetest-mod-antipvp and 
must be named as `antipvp`.

WE only use telegram so find us for support https://t.me/+dvlpBEDUq3gzMTcx

For more info we use all the tools of Final Minetest by oldcoder http://minetest.org

### Configuration

You can set those configurations in the mod tab at the gui, or added those option 
to the minetest config file at game directory, to the main global config file.

| config option       | type | default | description |
| ------------------- | ---- | ------- | ----------- |
| antipvp_kill_hitter | bool | false   | if enable, players that hit will be killed. |
| antipvp_admin_privs | bool | true    | if enable, the admin of the server will be automaticaly with antipvp privilegies. |
| antipvp_send_notify | bool | true    | if enable, permit to send notifications chat messages to involved players, check the next setting for details |
| antipvp_send_to_all | bool | false   | if enable, when a hitter try to hurt to a invulnerable player, send the message to all players. |
| antipvp_area_ratio  | int  | 2       | number in blocks to define peacefull antipvp area around the antipvp block. |

### Nodes

The `antipvp:quiet` block is a block that allows you to define an area out of damage 
from other players, depending on how the radius of `antipvp_area_ratio` is configured, 
**it can be helfully, a danger or a trap on anarchic servers**.

### crafting

```
 { "default:obsidianbrick", "default:goldblock", "default:obsidianbrick" },
 { "default:goldblock",     "flowers:rose",          "default:goldblock" },
 { "default:obsidianbrick", "default:goldblock", "default:obsidianbrick" },
```

### Related mods

* the antipvp mod from BrunoMine only provide a simple area using the nodebox, this 
mod is fully compatible with and do not cause conflicts
* the pvpinvul mod from gnuhacker is just a setting over an admin player, it 
does not provide privilegies either do not permit configure the messages
* rest of anti pvp mods are based on areas, not in a node position configuration, 
this mos its like more like protection area node boxes
* the idea of this mod is to be more equilibrate to permit a hope but not promote lazy 
over the players, so will mantain those layer keep on moving

If you are interested in more mods there are a huge amount of in the historical repo 
of the real minetest page at http://minetest.org

## License

   Copyright (C) 2022 PICCORO Lenz McKAY

Some portion and first base code were posible thanks to:

   Copyright (C) ???? Sandro Del Toro De Ana

￼ ￼ ￼ ￼
Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
only due sources were lost and the mod is rewriten mostly to newer functions, 
check the file [LICENSE](LICENSE)

Any portion of code of firs 2 commit are under GPLv3, but that code was lost.

The right place to check this development is the real minetest site minetest.org
and is repo is at https://git.minetest.io/minenux/minetest-mod-antipvp

